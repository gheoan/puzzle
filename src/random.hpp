struct XorShiftRng {
  u32 state;

  XorShiftRng static seed_from(u32);

  u32 next();
  u32 between(u32, u32);
};