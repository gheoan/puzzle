struct Square {
  bool revealed;
  bool flag;
  bool mine;
  // adjacent mines count
  u8 adjacentCount;
  sf::ConvexShape shape;
  sf::Text text;
  sf::Text flag_text;

  Square() : shape(0) {}
};

const u8 SQUARE_COUNT = 12;
const u8 MINE_COUNT = 13;

#define MINE_COLOR (sf::Color(242, 63, 56))

struct square_iterator {
  std::array<std::array<Square, SQUARE_COUNT>, SQUARE_COUNT>* grid;
  size_t i;
  size_t j;
  Square* square;
};

square_iterator iterate_squares(
    std::array<std::array<Square, SQUARE_COUNT>, SQUARE_COUNT>* grid) {
  square_iterator result = {};
  result.square = &(*grid)[0][0];
  result.grid = grid;
  return result;
}

void advance(square_iterator* iterator) {
  iterator->square = 0;

  if (iterator->j + 1 < SQUARE_COUNT) {
    iterator->j += 1;
    iterator->square = &(*iterator->grid)[iterator->i][iterator->j];
  } else if (iterator->i + 1 < SQUARE_COUNT) {
    iterator->j = 0;
    iterator->i += 1;
    iterator->square = &(*iterator->grid)[iterator->i][iterator->j];
  }
}

void loopMines(sf::RenderWindow& window, sf::Font* font, XorShiftRng* rng) {
  enum ProgressType {
    playing,
    won,
    lost,
  };

  std::array<std::array<Square, SQUARE_COUNT>, SQUARE_COUNT> grid = {};
  ProgressType progress = playing;

  for (auto iter = iterate_squares(&grid); iter.square; advance(&iter)) {
    auto s = iter.square;
    auto shape = &s->shape;
    build_rounded_rectangle(shape, {SQUARE_WIDTH, SQUARE_WIDTH}, 5.);
    shape->setPosition({X_OFFSET + iter.i * (SQUARE_WIDTH + SQUARE_OFFSET),
                        Y_OFFSET + iter.j * (SQUARE_WIDTH + SQUARE_OFFSET)});
    s->shape.setOutlineThickness(-1.);
    auto outline_color = BUTTON_COLOR + BACKGROUND_COLOR;
    outline_color.a = outline_color.a * 0.6;
    s->shape.setOutlineColor(outline_color);
    s->text.setFont(*font);
    s->text.setCharacterSize(FONT_SIZE);
    s->text.setFillColor(BACKGROUND_COLOR);
    s->flag_text.setFont(*font);
    s->flag_text.setCharacterSize(FONT_SIZE);
    s->flag_text.setFillColor(BACKGROUND_COLOR);
    s->flag_text.setString("F");
    sf::FloatRect textRect = s->flag_text.getLocalBounds();
    s->flag_text.setOrigin(textRect.width / 2.f, 0.f);
    s->flag_text.setPosition(s->shape.getPosition() +
                             sf::Vector2f(SQUARE_WIDTH / 2.f, 0.));
  }

  // place mines
  for (u8 m = 0; m < MINE_COUNT; m++) {
    u8 x;
    u8 y;
    for (;;) {
      x = rng->between(0, SQUARE_COUNT - 1);
      y = rng->between(0, SQUARE_COUNT - 1);
      if (grid[x][y].mine == false) {
        grid[x][y].mine = true;
        break;
      }
    }
  }

  // calculate adjacent mines count
  for (auto iter = iterate_squares(&grid); iter.square; advance(&iter)) {
    auto square = iter.square;
    auto i = iter.i;
    auto j = iter.j;
    if (!square->mine) {
      if (i >= 1 && j >= 1 && grid[i - 1][j - 1].mine) {
        square->adjacentCount += 1;
      }
      if (j >= 1 && grid[i][j - 1].mine) {
        square->adjacentCount += 1;
      }
      if (i + 1 <= SQUARE_COUNT - 1 && j >= 1 && grid[i + 1][j - 1].mine) {
        square->adjacentCount += 1;
      }
      if (i + 1 <= SQUARE_COUNT - 1 && grid[i + 1][j].mine) {
        square->adjacentCount += 1;
      }
      if (i + 1 <= SQUARE_COUNT - 1 && j + 1 <= SQUARE_COUNT - 1 &&
          grid[i + 1][j + 1].mine) {
        square->adjacentCount += 1;
      }
      if (j + 1 <= SQUARE_COUNT - 1 && grid[i][j + 1].mine) {
        square->adjacentCount += 1;
      }
      if (i >= 1 && j + 1 <= SQUARE_COUNT - 1 && grid[i - 1][j + 1].mine) {
        square->adjacentCount += 1;
      }
      if (i >= 1 && grid[i - 1][j].mine) {
        square->adjacentCount += 1;
      }
      if (square->adjacentCount != 0) {
        char adjacentCStr[2];
        sprintf(adjacentCStr, "%i", square->adjacentCount);
        square->text.setString(adjacentCStr);
        sf::FloatRect textRect = square->text.getLocalBounds();
        square->text.setOrigin(textRect.width / 2.f, 0.f);
        square->text.setPosition(square->shape.getPosition() +
                                 sf::Vector2f(SQUARE_WIDTH / 2.f, 0.));
      }
    }
  }

  Button closeButton("Back");
  auto closeButtonSize = closeButton.rect.getLocalBounds();
  closeButton.setPosition(
      {window.getSize().x - closeButtonSize.width - X_OFFSET, Y_OFFSET});

  sf::Text resultText = text_new();

  sf::Event event;
  while (window.isOpen()) {
    while (window.pollEvent(event)) {
      switch (event.type) {
        case sf::Event::Closed: {
          window.close();
          break;
        }
        case sf::Event::MouseButtonReleased: {
          if (closeButton.clicked(&event)) {
            return;
          }

          // if won or lost
          if (progress != playing) {
            break;
          }

          size_t x;
          size_t y;
          bool found = false;
          for (auto iter = iterate_squares(&grid); iter.square;
               advance(&iter)) {
            if (iter.square->shape.getGlobalBounds().contains(
                    (f32)event.mouseButton.x, (f32)event.mouseButton.y)) {
              x = iter.i;
              y = iter.j;
              found = true;
              break;
            }
          }

          // ignore if not clicked on a square
          if (!found) {
            break;
          }
          if (event.mouseButton.button == sf::Mouse::Left) {
            if (grid[x][y].mine) {
              progress = lost;
            } else {
              // squares that must be revealed
              std::vector<std::array<size_t, 2>> pending;
              pending.push_back({x, y});
              while (pending.size()) {
                size_t i = pending.back()[0];
                size_t j = pending.back()[1];
                pending.pop_back();
                if (grid[i][j].revealed || grid[i][j].mine) {
                  continue;
                }
                grid[i][j].revealed = true;
                if (grid[i][j].adjacentCount > 0) {
                  continue;
                }

                // add adjacent square to queue
                if (i >= 1 && j >= 1) {
                  pending.push_back({i - 1, j - 1});
                }
                if (j >= 1) {
                  pending.push_back({i, j - 1});
                }
                if (i + 1 <= SQUARE_COUNT - 1 && j >= 1) {
                  pending.push_back({i + 1, j - 1});
                }
                if (i + 1 <= SQUARE_COUNT - 1) {
                  pending.push_back({i + 1, j});
                }
                if (i + 1 <= SQUARE_COUNT - 1 && j + 1 <= SQUARE_COUNT - 1) {
                  pending.push_back({i + 1, j + 1});
                }
                if (j + 1 <= SQUARE_COUNT - 1) {
                  pending.push_back({i, j + 1});
                }
                if (i >= 1 && j + 1 <= SQUARE_COUNT - 1) {
                  pending.push_back({i - 1, j + 1});
                }
                if (i >= 1) {
                  pending.push_back({i - 1, j});
                }
              }

              // check if only mines are left on grid
              progress = won;
              for (auto iter = iterate_squares(&grid); iter.square;
                   advance(&iter)) {
                if (!iter.square->revealed && !iter.square->mine) {
                  progress = playing;
                  break;
                }
              }
            }
          } else if (event.mouseButton.button == sf::Mouse::Right) {
            if (!grid[x][y].revealed) {
              grid[x][y].flag = !grid[x][y].flag;
            }
          }

          for (auto iter = iterate_squares(&grid); iter.square;
               advance(&iter)) {
            if (progress == won || (progress == lost && iter.square->mine)) {
              iter.square->revealed = true;
            }
          }
          break;
        }
        default: {
          break;
        }
      }
    }

    // display the grid
    window.clear(BACKGROUND_COLOR);

    for (auto iter = iterate_squares(&grid); iter.square; advance(&iter)) {
      auto square = iter.square;
      if (square->revealed) {
        if (square->mine) {
          square->shape.setFillColor(MINE_COLOR);
        } else {
          square->shape.setFillColor(REVEALED_COLOR);
        }
      } else if (square->flag) {
        square->shape.setFillColor(FLAG_COLOR);
      } else {
        square->shape.setFillColor(UNREVEALED_COLOR);
      }
      window.draw(square->shape);

      // draw text (over squares)
      if (square->revealed && !square->mine && square->adjacentCount) {
        window.draw(square->text);
      } else if (square->flag) {
        window.draw(square->flag_text);
      }
    }

    closeButton.draw(&window);

    if (progress != playing) {
      const char* s;
      if (progress == won) {
        s = "You won!";
      } else {
        s = "You lost!";
      }
      resultText.setString(s);
      text_set_position(&resultText,
                        {window.getSize().x - X_OFFSET, Y_OFFSET + 100});
      window.draw(resultText);
    }

    window.display();
  }
}
