Button::Button(sf::Vector2f position, sf::String str) : Button(str) {
  setPosition(position);
}

Button::Button(sf::String str) {
  build_rounded_rectangle(
      &rect,
      sf::Vector2f(str.getSize() * FONT_SIZE / 2.f + BUTTON_PADDING.x,
                   25.f + BUTTON_PADDING.y),
      3);
  rect.setFillColor(BUTTON_COLOR);
  rect.setOutlineThickness(-1.f);
  auto outline_color = BUTTON_COLOR + BACKGROUND_COLOR;
  outline_color.a = outline_color.a * 0.6;
  rect.setOutlineColor(outline_color);
  text.setFont(*global_font_ptr);
  text.setFillColor(TEXT_COLOR);
  text.setString(str);
  text.setCharacterSize(FONT_SIZE);
  text.setLineSpacing(0);
  text.setOrigin(0, 12.f);
}

void Button::setPosition(sf::Vector2f position) {
  rect.setPosition(position);
  text.setPosition(rect.getPosition() + BUTTON_PADDING / 2.f);
}

void Button::draw(sf::RenderWindow* window) {
  window->draw(rect);
  window->draw(text);
}

bool Button::clicked(sf::Event* event) {
  return rect.getGlobalBounds().contains((f32)event->mouseButton.x,
                                         (f32)event->mouseButton.y);
}