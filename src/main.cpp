#include "main.hpp"
sf::Font* global_font_ptr;

inline f32 atan2(sf::Vector2f v) {
  const f32 pi = 3.141592654f;
  auto result = atan2(v.y, v.x);
  if (result < 0) {
    result = result + 2 * pi;
  }
  return result;
}

void build_rounded_rectangle(sf::ConvexShape* shape, sf::Vector2f size,
                             f32 radius) {
  sf::RectangleShape rect;
  rect.setSize(size - 2.f * sf::Vector2(radius, radius));
  sf::CircleShape circ;
  circ.setRadius(radius);

  auto rect_p_count = rect.getPointCount();
  auto circle_p_count = circ.getPointCount();
  shape->setPointCount(rect_p_count + circle_p_count);

  size_t rect_i = 0;
  size_t circle_i = 0;
  auto i = 0;
  auto k = 0;
  for (; i < (rect_p_count + circle_p_count - k); i += 1) {
    auto rect_p = rect.getPoint(rect_i % rect_p_count);
    auto circle_p = circ.getPoint(circle_i % circle_p_count);
    shape->setPoint(i, rect_p + circle_p);
    auto next_rect_p = rect.getPoint((rect_i + 1) % rect_p_count);
    auto rect_angle = atan2(next_rect_p - rect_p);
    auto next_circle_p = circ.getPoint((circle_i + 1) % circle_p_count);
    auto circle_angle = atan2(next_circle_p - circle_p);
    if (rect_angle < circle_angle) {
      rect_i += 1;
    } else if (circle_angle < rect_angle) {
      circle_i += 1;
    } else {
      // TODO: f32 equality
      rect_i += 1;
      circle_i += 1;
      k += 1;
    }
    if (rect_i > rect_p_count) {
      // TODO: can we not increase the index further then rect_p_count in the
      // first place?
      rect_i = rect_p_count;
      circle_i += 1;
    }
    if (circle_i > circle_p_count) {
      circle_i = circle_p_count;
      rect_i += 1;
    }
  }
  shape->setPointCount(i);
}

sf::Text text_new() {
  sf::Text t;
  t.setFont(*global_font_ptr);
  t.setFillColor(TEXT_COLOR);
  t.setCharacterSize(FONT_SIZE);
  t.setOrigin(0, 12.f);

  return t;
}

sf::Text text_new(const char* str) {
  sf::Text t = text_new();
  t.setString(str);

  return t;
}

void text_set_position(sf::Text* text, sf::Vector2f position) {
  position.x -= text->getString().getSize() * FONT_SIZE / 2.f;
  text->setPosition(position + BUTTON_PADDING / 2.f);
}

#include "alphabet.cpp"
#include "button.cpp"
#include "mines.cpp"
#include "random.cpp"

int main() {
  sf::Clock timer;
  sf::Font global_font;
  if (!global_font.loadFromFile("ter-u32n.bdf")) {
    std::cout << "Could not find font in " << std::filesystem::current_path()
              << "." << std::endl;
    std::cin.get();
    return -1;
  }

  global_font_ptr = &global_font;

  sf::VideoMode video_mode(640, 480);
  auto video_modes = sf::VideoMode::getFullscreenModes();
  if (!video_modes.empty()) {
    f32 best_ratio = (f32)video_modes[0].width / (f32)video_modes[0].height;
    auto best_height = (u32)(80. / 100. * (f32)video_modes[0].height);
    auto best_width = (u32)(80. / 100. * (f32)video_modes[0].width);
    for (auto i = 0; i < video_modes.size(); i += 1) {
      f32 ratio = (f32)video_modes[i].width / (f32)video_modes[i].height;
      if (video_modes[i].height <= best_height &&
          video_modes[i].width <= best_width &&
          fabs(best_ratio - ratio) < 0.5) {
        video_mode = video_modes[i];
        break;
      }
    }
  }

  sf::ContextSettings ctxSettings;
  ctxSettings.antialiasingLevel = 8;
  sf::RenderWindow window(video_mode, "Puzzles",
                          sf::Style::Titlebar | sf::Style::Close, ctxSettings);
  window.setFramerateLimit(15);

  Button minesButton({100.f, 100.f}, "Play Mines");
  Button alphabetButton({100.f, 150.f}, "Play Alphabet in Grid");
  Button closeButton({100.f, 200.f}, "Quit");

  auto rng =
      XorShiftRng::seed_from((u32)timer.getElapsedTime().asMicroseconds());

  sf::Event event;
  while (window.isOpen()) {
    while (window.pollEvent(event)) {
      switch (event.type) {
        case sf::Event::Closed: {
          return 0;
        }
        case sf::Event::MouseButtonReleased: {
          if (event.mouseButton.button != sf::Mouse::Left) {
            break;
          }
          if (minesButton.clicked(&event)) {
            loopMines(window, global_font_ptr, &rng);
          }
          if (alphabetButton.clicked(&event)) {
            loopAlphabet(window, global_font_ptr, &rng);
          }
          if (closeButton.clicked(&event)) {
            return 0;
          }
          break;
        }
        default:
          break;
      }
    }

    window.clear(BACKGROUND_COLOR);

    minesButton.draw(&window);
    alphabetButton.draw(&window);
    closeButton.draw(&window);

    window.display();
  }
}
