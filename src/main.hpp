#define _CRT_SECURE_NO_WARNINGS
#include <SFML/Graphics.hpp>
#include <array>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <filesystem>
#include <iostream>
#include <optional>
#include <random>
#include <string>
#include <unordered_set>
#include <vector>

typedef char c8;
typedef int8_t i8;
typedef uint8_t u8;
typedef int16_t i16;
typedef uint16_t u16;
typedef int32_t i32;
typedef uint32_t u32;
typedef int64_t i64;
typedef uint64_t u64;
typedef intptr_t isize;
typedef uintptr_t usize;
typedef float f32;
typedef double f64;

#include "button.hpp"
#include "random.hpp"

const u32 FONT_SIZE = 32;
#define BACKGROUND_COLOR (sf::Color(53, 61, 64))
#define TEXT_COLOR (sf::Color(219, 219, 219))
#define UNREVEALED_COLOR BUTTON_COLOR
#define REVEALED_COLOR (sf::Color(161, 165, 166))
#define FLAG_COLOR (sf::Color(242, 177, 56))
