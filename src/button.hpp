#define BUTTON_COLOR (sf::Color(0, 63, 99))
#define BUTTON_PADDING (sf::Vector2f(10.f, 15.f))

struct Button {
  sf::ConvexShape rect;
  sf::Text text;

  Button(sf::Vector2f, sf::String);
  Button(sf::String);

  void setPosition(sf::Vector2f);
  void draw(sf::RenderWindow*);

  bool clicked(sf::Event*);
};