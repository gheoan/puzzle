const u8 columnCount = 6;
const u8 lineCount = 5;
const f32 SQUARE_OFFSET = 3.f;
const f32 SQUARE_WIDTH = 40.f;
const f32 gridOffset = SQUARE_WIDTH;
const f32 X_OFFSET = SQUARE_WIDTH;
const f32 Y_OFFSET = 60.f;

void setAlphabetTextStyle(sf::Text& text, sf::Font& font) {
  text.setFont(font);
  text.setFillColor(BACKGROUND_COLOR);
  text.setCharacterSize(FONT_SIZE);
}

void loopAlphabet(sf::RenderWindow& window, sf::Font* font, XorShiftRng* rng) {
  struct square {
    sf::ConvexShape shape;
    sf::Text text;

    // true if this square is in a corner
    bool empty;
    std::optional<char> letter;
    // interior clue
    bool clue;
    bool exterior_clue;
  };
  std::vector<std::vector<square>> grid;

  enum ProgressType {
    playing,
    won,
  };
  ProgressType progress = playing;

  // initialize the grid
  for (u8 i = 0; i < lineCount; i += 1) {
    std::vector<square> line;
    for (u8 j = 0; j < columnCount; j += 1) {
      square s;
      s.clue = false;
      s.exterior_clue = false;
      bool empty = false;
      if ((i == 0 && j == 0) || (i == 0 && j == columnCount - 1) ||
          (i == lineCount - 1 && j == 0) ||
          (i == lineCount - 1 && j == columnCount - 1)) {
        empty = true;
      }
      s.empty = empty;
      s.letter = std::nullopt;
      sf::ConvexShape shape;
      build_rounded_rectangle(&shape, {SQUARE_WIDTH, SQUARE_WIDTH}, 10.);
      shape.setOutlineThickness(-SQUARE_OFFSET);
      shape.setOutlineColor(BACKGROUND_COLOR);
      shape.setPosition(X_OFFSET + gridOffset + j * SQUARE_WIDTH,
                        Y_OFFSET + gridOffset + i * SQUARE_WIDTH);
      s.shape = shape;
      sf::Text text = {};
      setAlphabetTextStyle(text, *font);
      text.setPosition(s.shape.getPosition() +
                       sf::Vector2f(SQUARE_WIDTH / 2.f, 0.));
      s.text = text;
      line.push_back(s);
    }
    grid.push_back(line);
  }

  // create a valid solution
  // TODO: sometimes a clue is generated that is placed in the grid too far from
  // its alphabet order neighbour. such a level is can not be solved
  char letterCounter;
  sf::Vector2<u8> letterPos;
  bool valid;
  do {
    // delete letters from previous attempt
    for (u8 y = 0; y < lineCount; y += 1) {
      for (u8 x = 0; x < columnCount; x += 1) {
        grid[y][x].letter = std::nullopt;
      }
    }
    do {
      letterPos = sf::Vector2<u8>(rng->between(0, columnCount - 1),
                                  rng->between(0, lineCount - 1));
    } while (grid[letterPos.y][letterPos.x].empty);
    valid = true;
    letterCounter = 'A';
    for (;;) {
      grid[letterPos.y][letterPos.x].letter = letterCounter;
      grid[letterPos.y][letterPos.x].text.setString(letterCounter);
      sf::FloatRect textRect =
          grid[letterPos.y][letterPos.x].text.getLocalBounds();
      grid[letterPos.y][letterPos.x].text.setOrigin(textRect.width / 2.f, 0.);
      if (letterCounter == 'Z') {
        break;
      }
      letterCounter += 1;
      std::unordered_set<u8> possibilities;
      if (letterPos.x < columnCount - 1 &&
          !grid[letterPos.y][letterPos.x + 1].letter &&
          !grid[letterPos.y][letterPos.x + 1].empty) {
        possibilities.insert(0);
      }
      if (letterPos.x > 0 && !grid[letterPos.y][letterPos.x - 1].letter &&
          !grid[letterPos.y][letterPos.x - 1].empty) {
        possibilities.insert(1);
      }
      if (letterPos.y < lineCount - 1 &&
          !grid[letterPos.y + 1][letterPos.x].letter &&
          !grid[letterPos.y + 1][letterPos.x].empty) {
        possibilities.insert(2);
      }
      if (letterPos.y > 0 && !grid[letterPos.y - 1][letterPos.x].letter &&
          !grid[letterPos.y - 1][letterPos.x].empty) {
        possibilities.insert(3);
      }
      if (!(letterPos.y == 1 && letterPos.x == 0) &&
          !(letterPos.y == 4 && letterPos.x == 4) &&
          letterPos.x < columnCount - 1 && letterPos.y > 0 &&
          !grid[letterPos.y - 1][letterPos.x + 1].letter &&
          !grid[letterPos.y - 1][letterPos.x + 1].empty) {
        possibilities.insert(4);
      }
      if (!(letterPos.y == 0 && letterPos.x == 4) &&
          !(letterPos.y == 3 && letterPos.x == 0) &&
          letterPos.x < columnCount - 1 && letterPos.y < lineCount - 1 &&
          !grid[letterPos.y + 1][letterPos.x + 1].letter &&
          !grid[letterPos.y + 1][letterPos.x + 1].empty) {
        possibilities.insert(5);
      }
      if (!(letterPos.y == 1 && letterPos.x == 5) &&
          !(letterPos.y == 4 && letterPos.x == 1) && letterPos.x > 0 &&
          letterPos.y > 0 && !grid[letterPos.y - 1][letterPos.x - 1].letter &&
          !grid[letterPos.y - 1][letterPos.x - 1].empty) {
        possibilities.insert(6);
      }
      if (!(letterPos.y == 0 && letterPos.x == 1) &&
          !(letterPos.y == 3 && letterPos.x == 5) && letterPos.x > 0 &&
          letterPos.y < lineCount - 1 &&
          !grid[letterPos.y + 1][letterPos.x - 1].letter &&
          !grid[letterPos.y + 1][letterPos.x - 1].empty) {
        possibilities.insert(7);
      }
      if (possibilities.size()) {
        u8 i;
        do {
          i = rng->between(0, 7);
        } while (possibilities.find(i) == possibilities.end());
        switch (i) {
          case 0: {
            letterPos.x += 1;
            break;
          }
          case 1: {
            letterPos.x -= 1;
            break;
          }
          case 2: {
            letterPos.y += 1;
            break;
          }
          case 3: {
            letterPos.y -= 1;
            break;
          }
          case 4: {
            letterPos.x += 1;
            letterPos.y -= 1;
            break;
          }
          case 5: {
            letterPos.x += 1;
            letterPos.y += 1;
            break;
          }
          case 6: {
            letterPos.x -= 1;
            letterPos.y -= 1;
            break;
          }
          case 7: {
            letterPos.x -= 1;
            letterPos.y += 1;
            break;
          }
          default:
            break;
        }
      } else {
        valid = false;
        break;
      }
    }
  } while (!valid);

  grid[0][2].clue = true;
  grid[2][4].clue = true;
  grid[3][2].clue = true;
  grid[4][4].clue = true;

  grid[0][2].shape.setFillColor(BUTTON_COLOR);
  grid[2][4].shape.setFillColor(BUTTON_COLOR);
  grid[3][2].shape.setFillColor(BUTTON_COLOR);
  grid[4][4].shape.setFillColor(BUTTON_COLOR);

  grid[0][2].text.setFillColor(TEXT_COLOR);
  grid[2][4].text.setFillColor(TEXT_COLOR);
  grid[3][2].text.setFillColor(TEXT_COLOR);
  grid[4][4].text.setFillColor(TEXT_COLOR);

  const auto EXTERNAL_CLUE_COUNT = 3;
  sf::Text external_clues[EXTERNAL_CLUE_COUNT];
  for (auto i = 0; i < EXTERNAL_CLUE_COUNT; i += 1) {
    setAlphabetTextStyle(external_clues[i], *font);
    external_clues[i].setFillColor(TEXT_COLOR);
    external_clues[i].setPosition(X_OFFSET + SQUARE_WIDTH / 2.f,
                                  Y_OFFSET + gridOffset);
  }
  {
    u8 i = 0;
    u8 x = 2;
    u8 y;
    // TODO: bug - this external clue got displayed with the letter of an
    // interior clue
    do {
      y = rng->between(0, columnCount - 1);
    } while (grid[x][y].clue);
    grid[x][y].exterior_clue = true;
    external_clues[i].setPosition(external_clues[i].getPosition() +
                                  sf::Vector2f(0.f, x * SQUARE_WIDTH));

    external_clues[i].setString(*grid[x][y].letter);
    sf::FloatRect textRect = external_clues[i].getLocalBounds();
    external_clues[i].setOrigin(textRect.width / 2.f, 0.f);
  }
  {
    u8 i = 1;
    u8 x = 3;
    u8 y;
    do {
      y = rng->between(0, columnCount - 1);
    } while (grid[x][y].clue || grid[x][y].exterior_clue);
    grid[x][y].exterior_clue = true;
    external_clues[i].setPosition(external_clues[i].getPosition() +
                                  sf::Vector2f(0.f, x * SQUARE_WIDTH));

    external_clues[i].setString(*grid[x][y].letter);
    sf::FloatRect textRect = external_clues[i].getLocalBounds();
    external_clues[i].setOrigin(textRect.width / 2.f, 0.f);
  }
  {
    u8 i = 2;
    u8 x;
    u8 y = 2;
    do {
      x = rng->between(0, lineCount - 1);
    } while (grid[x][y].clue || grid[x][y].exterior_clue);
    grid[x][y].exterior_clue = true;
    external_clues[i].setPosition(
        external_clues[i].getPosition() +
        sf::Vector2f(y * SQUARE_WIDTH, lineCount * SQUARE_WIDTH));

    external_clues[i].setString(*grid[x][y].letter);
    sf::FloatRect textRect = external_clues[i].getLocalBounds();
    external_clues[i].setOrigin(textRect.width / 2.f, 0.f);
  }

  for (u8 x = 0; x < lineCount; x++) {
    for (u8 y = 0; y < columnCount; y++) {
      if (!grid[x][y].clue) {
        grid[x][y].letter = std::nullopt;
      }
    }
  }

  Button closeButton("Back");
  auto closeButtonSize = closeButton.rect.getLocalBounds();
  closeButton.setPosition(
      {window.getSize().x - closeButtonSize.width - X_OFFSET,
       Y_OFFSET + closeButtonSize.height});

  sf::Text wonText = text_new("You won!");
  text_set_position(&wonText, {window.getSize().x - X_OFFSET, Y_OFFSET + 100});

  std::optional<sf::Vector2<u8>> selected = std::nullopt;
  sf::Event event;
  while (window.isOpen()) {
    while (window.pollEvent(event)) {
      switch (event.type) {
        case sf::Event::Closed: {
          window.close();
          break;
        }
        case sf::Event::MouseButtonReleased: {
          if (closeButton.clicked(&event)) {
            return;
          }

          for (u8 i = 0; i < lineCount; i++) {
            for (u8 j = 0; j < columnCount; j++) {
              if (progress == playing &&
                  grid[i][j].shape.getGlobalBounds().contains(
                      (f32)event.mouseButton.x, (f32)event.mouseButton.y) &&
                  !grid[i][j].empty && !grid[i][j].clue) {
                selected = sf::Vector2(i, j);
                break;
              }
            }
          }
          break;
        }
        case sf::Event::TextEntered: {
          if (progress == playing && selected && event.text.unicode >= 'A' &&
              event.text.unicode <= 'Z') {
            grid[(*selected).x][(*selected).y].text.setString(
                event.text.unicode);
            grid[(*selected).x][(*selected).y].letter =
                static_cast<char>(event.text.unicode);

            progress = won;
            for (u8 y = 0; y < lineCount && progress == won; y++) {
              for (u8 x = 0; x < columnCount; x++) {
                if (grid[y][x].empty) {
                  continue;
                }
                if (!grid[y][x].letter) {
                  progress = playing;
                  break;
                }
                if (*grid[y][x].letter == 'Z') {
                  continue;
                }
                char letter = *grid[y][x].letter + 1;
                bool gasit = false;
                if (y > 0 && *grid[y - 1][x].letter == letter) {
                  gasit = true;
                }
                if (y < lineCount - 1 && *grid[y + 1][x].letter == letter) {
                  gasit = true;
                }
                if (x > 0 && *grid[y][x - 1].letter == letter) {
                  gasit = true;
                }
                if (x < columnCount - 1 && *grid[y][x + 1].letter == letter) {
                  gasit = true;
                }
                if (x > 0 && y > 0 && *grid[y - 1][x - 1].letter == letter) {
                  gasit = true;
                }
                if (y < lineCount - 1 && x < columnCount - 1 &&
                    *grid[y + 1][x + 1].letter == letter) {
                  gasit = true;
                }
                if (y > 0 && x < columnCount - 1 &&
                    *grid[y - 1][x + 1].letter == letter) {
                  gasit = true;
                }
                if (y < lineCount - 1 && x > 0 &&
                    *grid[y + 1][x - 1].letter == letter) {
                  gasit = true;
                }
                if (!gasit) {
                  progress = playing;
                  break;
                }
              }
            }

            if (progress == won) {
              selected = std::nullopt;
            }
          }
          break;
        }
        default: {
          break;
        }
      }
    }

    window.clear(BACKGROUND_COLOR);
    for (u8 y = 0; y < lineCount; y++) {
      for (u8 x = 0; x < columnCount; x++) {
        if (selected && y == (*selected).x && x == (*selected).y) {
          grid[y][x].shape.setFillColor(FLAG_COLOR);
        } else if (!grid[y][x].clue) {
          grid[y][x].shape.setFillColor(grid[y][x].empty ? BACKGROUND_COLOR
                                                         : REVEALED_COLOR);
        }
        window.draw(grid[y][x].shape);
        if (!grid[y][x].empty && (grid[y][x].clue || grid[y][x].letter)) {
          window.draw(grid[y][x].text);
        }
      }
    }

    for (u8 i = 0; i < 3; i++) {
      window.draw(external_clues[i]);
    }

    closeButton.draw(&window);

    if (progress == won) {
      window.draw(wonText);
    }

    window.display();
  }
}
