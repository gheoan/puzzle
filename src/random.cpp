XorShiftRng XorShiftRng::seed_from(u32 value) {
  XorShiftRng result = {};
  result.state = value;
  return result;
}

u32 XorShiftRng::next() {
  auto x = this->state;
  x ^= x << 13;
  x ^= x >> 17;
  x ^= x << 5;
  this->state = x;
  return x;
}

u32 XorShiftRng::between(u32 a, u32 b) {
  return a + this->next() % (b - a + 1);
}
