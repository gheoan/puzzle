# puzzle

A collection of singleplayer puzzles. The following games are implemented:
[Mines](https://en.wikipedia.org/wiki/Minesweeper_(video_game))
and
[Alphabet in Grid](http://www.geocities.ws/logic-club/PAGINI/puzzles/alfabet-s).

![Mines](./screenshots/mines.png)
![Alphabet](./screenshots/alphabet.png)